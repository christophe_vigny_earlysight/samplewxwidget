# Compile Project for Windows

# Disable strict ansi to let gtest use older function names
add_definitions(-U__STRICT_ANSI__)

# Use MinGW W64 over any other installed compilers
set(CMAKE_C_COMPILER   C:/msys64/mingw64/bin/gcc.exe)
set(CMAKE_CXX_COMPILER C:/msys64/mingw64/bin/g++.exe)
