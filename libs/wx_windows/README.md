 
wxWidgets 3.1.4

x86 Windows
MinGW gcc 9.2.0

Built from Git Bash terminal to generate makefile from configure script:  
```
mkdir wx.git
git clone https://github.com/wxWidgets/wxWidgets .
git fetch --tags
git checkout tags/v3.1.4 -b v3.1.4-release-tag
git submodule update --init
mkdir build_windows
../configure --disable-dependency-tracking
mingw32-make -j8
```

 - `include` : copied from wx.git  
 - `lib` : copied from build_windows
